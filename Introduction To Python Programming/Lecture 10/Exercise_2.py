People = 20
# 10 got off
People -= 10
# 3 come on
People += 3
# 15 come on
People += 15
# 5 come off
People -= 5
# plus 3 buses with 15 people in each
People += 15*3
print(People)

# if do it as one equtation:
People = 20 - 10 + 3 + 15 - 5 + 15*3
print(People)
